import { Menu } from './menu';

export const MENU: Menu[] = [
  { id: 11, name: 'abc' ,price: 100},
  { id: 12, name: 'def' ,price: 150},
  { id: 13, name: 'Bom' ,price: 200},
  { id: 14, name: 'Cel' ,price: 250},
  { id: 15, name: 'Mag' ,price: 280},
  { id: 16, name: 'Rub' ,price: 300},
  { id: 17, name: 'Dyn' ,price: 320},
  { id: 18, name: 'Dr ' ,price: 380},
  { id: 19, name: 'Mag' ,price: 400},
  { id: 20, name: 'Tor' ,price: 440}
];
export const MAIN: Main[] = [
  { id: 21, name: 'abc' ,price: 100},
  { id: 22, name: 'def' ,price: 150},
  { id: 23, name: 'Bom' ,price: 200},
  { id: 24, name: 'Cel' ,price: 250},
  { id: 25, name: 'Mag' ,price: 280},
  { id: 26, name: 'Rub' ,price: 300},
  { id: 27, name: 'Dyn' ,price: 320},
  { id: 28, name: 'Dr ' ,price: 380},
  { id: 29, name: 'Mag' ,price: 400},
  { id: 30, name: 'Tor' ,price: 440}
];
export const DESERTS: Deserts[] = [
  { id: 31, name: 'abc' ,price: 100},
  { id: 32, name: 'def' ,price: 150},
  { id: 33, name: 'Bom' ,price: 200},
  { id: 34, name: 'Cel' ,price: 250},
  { id: 35, name: 'Mag' ,price: 280},
  { id: 36, name: 'Rub' ,price: 300},
  { id: 37, name: 'Dyn' ,price: 320},
  { id: 38, name: 'Dr ' ,price: 380},
  { id: 39, name: 'Mag' ,price: 400},
  { id: 40, name: 'Tor' ,price: 440}
];